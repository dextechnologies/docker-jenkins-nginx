run:
	@cp ../api/requirements.txt api/requirements.txt
	@docker-compose up -d --build
stop:
	@docker-compose down
clean-images:
	@docker rmi $(docker images -q --filter="dangling=true")

